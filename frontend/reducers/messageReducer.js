import api from '../service/api';

export const REQUEST_MESSAGES = 'REQUEST_MESSAGES';
export const RECEIVE_MESSAGES = 'RECEIVE_MESSAGES';
export const     SEND_MESSAGE = 'SEND_MESSAGE';
export const     MESSAGE_SENT = 'MESSAGE_SENT';
export const     SET_INTERVAL = 'SET_INTERVAL';
export const   CLEAR_INTERVAL = 'CLEAR_INTERVAL';
/** data is
 * {
 *   name: string,
 *   userId: int
 * }
 *
*/
export function requestMessages() {
  return { type: REQUEST_MESSAGES }
}
export function receiveMessages(messages) {
  return { type: RECEIVE_MESSAGES, messages }
}
export function fetchMessages(chatId) {
  return (dispatch, getState) => {
    dispatch(requestMessages())
    const state = getState();
    const accessToken = state.auth.accessToken;
    return api.fetchMessages(accessToken, chatId)
      .then(response => response.json())
      .then(json => dispatch(receiveMessages(json.data)))
  }
}
export function setIntervalAction(intervalId) {
  return { type: SET_INTERVAL, intervalId }
}
export function clearIntervalAction() {
  return (dispatch, getState) => {
    const state = getState();
    const intervalId = state.message.intervalId;
    clearInterval(intervalId);
    dispatch({ type: CLEAR_INTERVAL })
  }
}
export function fetchMessagesWithInterval(chatId, interval) {
  return (dispatch, getState) => {
    dispatch(requestMessages())
    const state = getState();
    const accessToken = state.auth.accessToken;
    const intervalId = setInterval(() => {
      api.fetchMessages(accessToken, chatId)
        .then(response => response.json())
        .then(json => dispatch(receiveMessages(json.data)))
    }, interval);
    dispatch(setIntervalAction(intervalId))
  }
}

export function sendMessage() {
  return { type: SEND_MESSAGE }
}
export function messageSent(message) {
  return { type: MESSAGE_SENT, message }
}
export function sendMessageAsync(data) {
  return (dispatch, getState) => {
    dispatch(sendMessage())
    const state = getState();
    const accessToken = state.auth.accessToken;
    return api.sendMessage(accessToken, data)
      .then(response => response.json())
      .then(json => dispatch(messageSent(json)))
  }
}

export default function messageReducer(state = {
  items: [],
  isFetching: false,
  intervalId: null
}, action) {
  switch(action.type) {
    case REQUEST_MESSAGES:
      return { ...state, isFetching: true }
    case RECEIVE_MESSAGES:
      return { ...state, isFetching: false, items: action.messages }
    case SEND_MESSAGE:
      return state
    case MESSAGE_SENT:
      return { ...state, items: [...state.items, action.message] }
    case SET_INTERVAL:
      return { ...state, intervalId: action.intervalId }
    case CLEAR_INTERVAL:
      return { ...state, intervalId: null }
    default: return state
  }
}
