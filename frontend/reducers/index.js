import { combineReducers } from 'redux';

import authReducer from './authReducer';
import chatReducer from './chatReducer';
import userReducer from './userReducer';
import messageReducer from './messageReducer'

const rootReducer = combineReducers({
  auth: authReducer,
  chat: chatReducer,
  user: userReducer,
  message: messageReducer
});

export default rootReducer;
