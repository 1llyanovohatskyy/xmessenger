import api from '../service/api';

export const  REQUEST_CHATS = 'REQUEST_CHATS';
export const  RECEIVE_CHATS = 'RECEIVE_CHATS';
export const    CREATE_CHAT = 'CREATE_CHAT';
export const   CHAT_CREATED = 'CHAT_CREATED';
export const   SET_CHAT_INTERVAL = 'SET_CHAT_INTERVAL';
export const CLEAR_CHAT_INTERVAL = 'CLEAR_CHAT_INTERVAL';
/** data is
 * {
 *   name: string,
 *   userId: int
 * }
 *
*/
export function requestChats() {
  return { type: REQUEST_CHATS }
}
export function receiveChats(chats) {
  return { type: RECEIVE_CHATS, chats }
}
export function fetchChats() {
  return (dispatch, getState) => {
    dispatch(requestChats())
    const state = getState();
    const accessToken = state.auth.accessToken;
    return api.fetchChats(accessToken)
      .then(response => response.json())
      .then(json => dispatch(receiveChats(json.data)))
  }
}

export function setChatIntervalAction(intervalId) {
  return { type: SET_CHAT_INTERVAL, intervalId }
}
export function clearChatIntervalAction() {
  return (dispatch, getState) => {
    const state = getState();
    const intervalId = state.chat.intervalId;
    clearInterval(intervalId)
    dispatch({ type: CLEAR_CHAT_INTERVAL })
  }
}
export function fetchChatsWithInterval(interval) {
  return (dispatch, getState) => {
    dispatch(requestChats())
    const state = getState();
    const accessToken = state.auth.accessToken;
    const intervalId = setInterval(() => {
      api.fetchChats(accessToken)
        .then(response => response.json())
        .then(json => dispatch(receiveChats(json.data)))
    }, interval);
    dispatch(setChatIntervalAction(intervalId))
  }
}

export function createChat() {
  return { type: CREATE_CHAT }
}
export function chatCreated(chat) {
  return { type: CHAT_CREATED, chat }
}
export function createChatAsync(data) {
  return (dispatch, getState) => {
    dispatch(createChat())
    const state = getState();
    const accessToken = state.auth.accessToken;
    return api.createChat(accessToken, data)
      .then(response => response.json())
      .then(json => dispatch(chatCreated(json)))
  }
}

export default function chatReducer(state = {
  items: [],
  isFetching: false,
  isCreating: false,
  intervalId: null
}, action) {
  switch(action.type) {
    case REQUEST_CHATS:
      return { ...state, isFetching: true }
    case RECEIVE_CHATS:
      return { ...state, isFetching: false, items: action.chats }
    case CREATE_CHAT:
      return { ...state, isCreating: true }
    case CHAT_CREATED:
      return { ...state, isCreating: false, items: [...state.items, action.chat] }
    case SET_CHAT_INTERVAL:
      return { ...state, intervalId: action.intervalId }
    case CLEAR_CHAT_INTERVAL:
      return { ...state, intervalId: null }
    default: return state
  }
}


