import api from '../service/api';

export const REQUEST_AUTH = 'REQUEST_AUTH';
export const AUTH_DONE = 'AUTH_DONE';
export const CLEAR_ACCESS_TOKEN = 'CLEAR_ACCESS_TOKEN';

export function requestAuth() {
  return { type: REQUEST_AUTH }
}
export function authDone(payload) {
  return {
    type: AUTH_DONE,
    data: {
      user: {
        id: payload['user']['_id'],
        email: payload['user']['email'],
        avatarUrl: payload['user']['avatarUrl']
      },
      accessToken: payload['accessToken']
    }
  }
}
export function register(email, password) {
  return dispatch => {
    dispatch(requestAuth())
    return api.register(email, password)
      .then(response => response.json())
      .then(json => dispatch(authDone({})))
  }
}
export function authenticate(email, password, navigation) {
  return dispatch => {
    dispatch(requestAuth())
    return api.authenticate(email, password)
      .then(response => response.json())
      .then(json => dispatch(authDone(json)))
      .then(_ => navigation.navigate('HomeStack'))
  }
}
export function clearAccessToken() {
  return { type: CLEAR_ACCESS_TOKEN }
}
export function signOut(navigation) {
  return dispatch => {
    dispatch(clearAccessToken())
    return new Promise((resolve, reject) => {
      navigation.navigate('SignIn')
      resolve({})
    })
  }
}

export default function authReducer(state = {
  user: {
    id: null,
    email: '',
    avatarUrl: ''
  },
  accessToken: '',
  isProcessing: false
}, action) {
  switch(action.type) {
    case REQUEST_AUTH:
      return { ...state, isProcessing: true }
    case AUTH_DONE:
      return { ...state, isProcessing: false, ...action.data }
    case CLEAR_ACCESS_TOKEN:
      return { ...state, accessToken: '' }
    default: return state
  }
}
