import api from '../service/api';

export const REQUEST_USERS = 'REQUEST_USERS';
export const RECEIVE_USERS = 'RECEIVE_USERS';
/** data is
 * {
 *   users: [userId, userId, ...]
 * }
 *
*/
export function requestUsers() {
  return { type: REQUEST_USERS }
}
export function receiveUsers(users) {
  return { type: RECEIVE_USERS, users }
}
export function fetchUsers() {
  return (dispatch, getState) => {
    dispatch(requestUsers())
    const state = getState();
    const accessToken = state.auth.accessToken;
    return api.fetchUsers(accessToken)
      .then(response => response.json())
      .then(json => dispatch(receiveUsers(json.data)))
  }
}

export default function userReducer(state = {
  items: [],
  isFetching: false,
}, action) {
  switch(action.type) {
    case REQUEST_USERS:
      return { ...state, isFetching: true }
    case RECEIVE_USERS:
      return { ...state, isFetching: false, items: action.users }
    default: return state
  }
}
