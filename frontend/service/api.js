const API_ENDPOINT = 'http://34.90.212.90';

const api = () => {
  const fetchChats = token => {
    return fetch(`${API_ENDPOINT}/userChats`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }

  const fetchUsers = token => {
    return fetch(`${API_ENDPOINT}/users`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }

  const fetchMessages = (token, chatId) => {
    return fetch(`${API_ENDPOINT}/userChats/${chatId}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }

  const register = (email, password) => {
    return fetch(`${API_ENDPOINT}/users`, {
      method: 'POST',
      body: JSON.stringify({email, password}),
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }

  const authenticate = (email, password) => {
    return fetch(`${API_ENDPOINT}/authentication`, {
      method: 'POST',
      body: JSON.stringify({strategy: 'local', email, password}),
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }

  const createChat = (token, data) => {
    return fetch(`${API_ENDPOINT}/userChats`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
  }

  const sendMessage = (token, data) => {
    return fetch(`${API_ENDPOINT}/messages`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
  }

  return {
    fetchChats,
    createChat,
    fetchUsers,
    fetchMessages,
    sendMessage,
    authenticate,
    register
  }
}


export default api()
