# XMessanger

XMessanger is a modern mobile and web based app for communication.

## Setup

Install [Node 12 LTS](https://nodejs.org/en/download/):
```bash
$ nvm install 12 && nvm use 12
```

Install project dependencies:
```bash
$ yarn install
```

To run app:
```bash
$ yarn start
```
Install the [Expo](https://expo.io/) client app on your iOS or Android phone and connect to the same wireless network as your computer. On Android, use the Expo app to scan the QR code from your terminal to open your project. On iOS, follow on-screen instructions to get a link.

