import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import ChatScreenNavigator from './ChatScreenNavigator';
import AuthStack from './AuthStack';

export default createAppContainer(
  createSwitchNavigator(
    {
      Main: MainTabNavigator,
      Chat: ChatScreenNavigator,
      SignIn: AuthStack.SignInStack,
      SignUp: AuthStack.SignUpStack
    },
    {
      initialRouteName: 'SignIn',
    }
  )
);
