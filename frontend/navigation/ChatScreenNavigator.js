import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Ionicons } from '@expo/vector-icons';

import ChatScreen from '../screens/ChatScreen';
import Colors from '../constants/Colors';

const ChatScreenNavigator = createStackNavigator({
  ChatScreen: {
    screen: ChatScreen,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.chatName}`,
      headerLeft: () => <Ionicons
                          name='md-arrow-back'
                          size={30}
                          onPress={() => navigation.navigate('HomeStack')}
                          style={{marginLeft: 20, color: Colors.headerTextColor }}
                        />
    })
  }
});

export default ChatScreenNavigator;
