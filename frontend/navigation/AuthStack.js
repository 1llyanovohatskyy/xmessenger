import { createStackNavigator } from 'react-navigation-stack';

import { HeaderOptions } from '../components/Header';
import SignInContainer from '../containers/SignInContainer';
import SignUpContainer from '../containers/SignUpContainer';

const SignInStack = createStackNavigator({
  SignIn: {
    screen: SignInContainer,
    navigationOptions: () => ({
      ...HeaderOptions({ title: 'XMessanger' })
    })
  }
});

const SignUpStack = createStackNavigator({
  SignUp: {
    screen: SignUpContainer,
    navigationOptions: () => ({
      ...HeaderOptions({ title: 'XMessanger' })
    })
  }
});

export default {
  SignInStack,
  SignUpStack
};
