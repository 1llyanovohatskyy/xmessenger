import React from 'react';
import { connect } from 'react-redux';

import SignInScreen from '../screens/SignInScreen';
import { authenticate } from '../reducers/authReducer'

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    navigation: ownProps.navigation,
    authenticate: (email, pass, nav) => dispatch(authenticate(email, pass, nav))
  }
};

export default connect(null, mapDispatchToProps)(SignInScreen);
