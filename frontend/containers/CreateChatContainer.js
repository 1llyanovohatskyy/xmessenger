import { connect } from 'react-redux';

import CreateChatForm from '../components/CreateChatForm';
import { createChatAsync } from '../reducers/chatReducer'
import { fetchUsers } from '../reducers/userReducer'

const mapDispatchToProps = dispatch => {
  dispatch(fetchUsers());
  return {
    createChat: data => dispatch(createChatAsync(data))
  }
};

const mapStateToProps = state => ({
  userItems: state.user.items.map(u => ({...u, key: u['_id']})),
  isFetching: state.user.isFetching
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateChatForm);
