import React from 'react';
import { connect } from 'react-redux';

import SignUpScreen from '../screens/SignUpScreen';
import { register, authenticate } from '../reducers/authReducer'

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    navigation: ownProps.navigation,
    register: (email, pass, nav) => {
      dispatch(register(email, pass))
      dispatch(authenticate(email, pass, nav))
    },
  }
};

export default connect(null, mapDispatchToProps)(SignUpScreen);
