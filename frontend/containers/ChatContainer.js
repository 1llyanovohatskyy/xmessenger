import React from 'react';
import { connect } from 'react-redux';

import Chat from '../components/Chat';
import {
  sendMessageAsync,
  fetchMessagesWithInterval,
  clearIntervalAction
} from '../reducers/messageReducer';

const mapDispatchToProps = (dispatch, ownProps) => {
  dispatch(fetchMessagesWithInterval(ownProps.navigation.getParam('chatId'), 2000))
  return {
    sendMessage: data => dispatch(sendMessageAsync(data)),
    cleanUp: () => dispatch(clearIntervalAction())
  }
};

const mapStateToProps = state => ({
  messages: state.message.items.map(c => ({...c, key: c['_id']})),
  isFetching: state.message.isFetching,
  user: state.auth.user
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
