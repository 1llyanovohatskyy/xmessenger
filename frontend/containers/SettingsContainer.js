import React from 'react';
import { connect } from 'react-redux';

import SettingsScreen from '../screens/SettingsScreen';
import { signOut } from '../reducers/authReducer'

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    signOut: () => dispatch(signOut(ownProps.navigation))
  }
};

export default connect(null, mapDispatchToProps)(SettingsScreen);
