import React from 'react';
import { connect } from 'react-redux';

import ChatList from '../components/ChatList';
import { fetchChatsWithInterval, clearChatIntervalAction } from '../reducers/chatReducer';

const mapDispatchToProps = dispatch => {
  dispatch(fetchChatsWithInterval(2000))
  return {
    cleanUp: () => dispatch(clearChatIntervalAction())
  }
};

const mapStateToProps = state => {
  return {
    chatItems: state.chat.items.map(c => ({...c, key: c['_id']})),
    isFetching: state.chat.isFetching
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatList);
