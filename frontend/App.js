import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';

import AppNavigator from './navigation/AppNavigator';
import { registerForPushNotificationsAsync } from './notifications';
import { store, persistor } from './store/store';

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  // registerForPushNotificationsAsync('SOME_USER_ID')
  //     .then(() => console.log('Token pushed!'))
  //     .catch(() => console.warn('Can not push token'));

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    // TODO: https://itnext.io/react-native-why-you-should-be-using-redux-persist-8ad1d68fa48b
    return(
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    )
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

