import thunkMiddleware from 'redux-thunk'
import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';

import rootReducer from '../reducers/index';

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware,
    createLogger(),
  ),
);

let persistor = persistStore(store);
export {
  store,
  persistor,
};
