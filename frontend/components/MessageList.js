import React from 'react';
import { FlatList } from 'react-native';
import Message from './Message';

export default function MessageList(props) {
  return (
    <FlatList
      data={props.messages}
      renderItem={({item}) => <Message
                                  text={item.text}
                                  authorId={item.authorId}
                                />
      }
    />
  )
}
