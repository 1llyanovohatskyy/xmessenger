import Colors from '../constants/Colors';

export function HeaderOptions(props) {
  return {
    title: props.title,
    headerStyle: {
      backgroundColor: Colors.tintColor,
    },
    headerTintColor: Colors.headerTextColor,
    headerTitleStyle: {
      fontWeight: 'bold'
    },
  }
}
