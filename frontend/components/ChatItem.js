import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { MonoText } from './StyledText';

function getHours(timeString) {
  return new Date(timeString).getHours()
}

function getMinutes(timeString) {
  return new Date(timeString).getMinutes()
}

export default function ChatItem(props) {
  const { id, name, lastMessage, unreadMessageCount } = props;
  return (
    <TouchableOpacity onPress={() => props.navigation.navigate('ChatScreen', { chatId: id, chatName: name }) }>
      <View style={styles.container}>
        <View style={styles.img}></View>
        <View style={styles.textDetails}>
          <MonoText style={styles.textDetailsHeadline}>{name}</MonoText>
          <MonoText style={styles.textDetailsLastMessage}>
            { lastMessage ? lastMessage.text : null }
          </MonoText>
        </View>
        <View style={styles.infoDetails}>
          <MonoText style={styles.lastMessageTime}>
            {
              lastMessage ?
                `${getHours(lastMessage.updatedAt)}:${getMinutes(lastMessage.updatedAt)}` : null
            }
          </MonoText>
          {
            unreadMessageCount ? (unreadMessageCount > 0 ?
              <MonoText style={styles.unreadMessageCount}>
                {props.unreadMessageCount}
            </MonoText> : null) : null
          }
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    paddingBottom: 0,
    fontSize: 18,
    borderBottomColor: 'black',
    borderBottomWidth: 0.5
  },
  img: {
    width: 40,
    height: 40,
    backgroundColor: 'steelblue',
    borderRadius: 20,
  },
  textDetails: {
    paddingLeft: 5,
    width: 212,
    height: 50,
  },
  textDetailsHeadline: {
    fontSize: 17,
  },
  textDetailsLastMessage: {
    fontSize: 13
  },
  infoDetails: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  lastMessageTime: {
  },
  unreadMessageCount: {
    backgroundColor: '#f3f7f8',
    marginTop: 5,
    width: 20,
    height: 20,
    borderRadius: 20,
    textAlign: 'center'
  }
})
