import React, { useEffect } from 'react';
import { FlatList } from 'react-native';
import ChatItem from './ChatItem';
import Spinner from './Spinner';

export default function ChatList(props) {
  const { isFetching, chatItems, navigation, cleanUp } = props;
  useEffect(() => {
    // returned function will be called on component unmount
    return () => cleanUp()
  }, [])
  if(isFetching) {
    return <Spinner />
  } else {
    return (
      <FlatList
        data={chatItems}
        renderItem={
          ({item}) => <ChatItem
                        id={item.key}
                        name={item.name}
                        navigation={navigation}
                        lastMessage={item.lastMessage}
                        unreadMessageCount={item.unreadMessageCount}
                      />
        }
      />
    )
  }
}
