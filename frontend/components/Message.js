import React from 'react';
import { View, StyleSheet } from 'react-native';
import { MonoText } from './StyledText';
import Colors from '../constants/Colors';

export default function Message(props) {
  const { text, authorId } = props;
  return (
    <View style={styles.container}>
      <MonoText>{text}</MonoText>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    width: 40,
    padding: 10,
    backgroundColor: Colors.tintColor,
    color: Colors.noticeText,
    fontSize: 18,
  }
})
