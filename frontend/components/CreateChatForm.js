import React, { useState } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Picker,
  Button,
} from 'react-native';
import Colors from '../constants/Colors';
import { MonoText } from './StyledText';
import Spinner from './Spinner';

export default function CreateChatForm(props) {
  const [name, setName] = useState('');
  const [userId, setUserId] = useState(null);
  const { isFetching, userItems, createChat } = props;

  if(isFetching) {
    return <Spinner />
  } else {
    return (
      <View style={styles.container}>
        <MonoText>Enter a chat name</MonoText>
        <TextInput
          style={styles.input}
          onChangeText={value => setName(value)}
          value={name}
        />
        <View style={styles.pickerSection}>
          <MonoText>Pick a person</MonoText>
          <Picker
            selectedValue={userId}
            style={styles.picker}
            onValueChange={value => setUserId(value)}
          >
            {
              userItems.map(user => <Picker.Item label={user.email} value={user.key} key={user.key} />)
            }
          </Picker>
        </View>
        <View style={styles.createButtonSection}>
          <Button
              title="Create"
              onPress={() => {
                createChat({ name, members: [userId] })
                setName('')
                setUserId(null)
              }}
              style={styles.createButton}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 20,
    paddingTop: 40,
    fontSize: 24,
  },
  input: {
    height: 50,
    borderTopColor: '#fff',
    borderLeftColor: '#fff',
    borderRightColor: '#fff',
    borderBottomColor: Colors.tintColor,
    borderWidth: 2,
    fontFamily: 'space-mono',
  },
  pickerSection: {
    marginTop: 40
  },
  createButtonSection: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 40
  },
  createButton: {
    backgroundColor: Colors.tintColor,
    color: Colors.noticeText,
  }
})
