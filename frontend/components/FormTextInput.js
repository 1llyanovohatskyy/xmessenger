import * as React from "react";
import { StyleSheet, TextInput } from "react-native";
import Colors from "../constants/Colors";

export default function FormTextInput(props) {
  const { style, ...otherProps } = props;
  return (
    <TextInput
      selectionColor={Colors.tintColor}
      style={[styles.textInput, style]}
      {...otherProps}
    />
  );
}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    borderColor: Colors.silver,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: 20
  }
});
