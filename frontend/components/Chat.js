import React, { useEffect } from 'react';
import { Platform } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from './Spinner';

export default function Chat(props) {
  const {
    messages,
    isFetching,
    sendMessage,
    navigation,
    cleanUp
  } = props;

  useEffect(() => {
    // returned function will be called on component unmount
    return () => cleanUp()
  }, [])

  const OnSend = msgs => {
    const lastMessage = msgs[msgs.length-1];
    GiftedChat.append(messages, msgs);
    sendMessage({ text: lastMessage.text, chatId: navigation.getParam('chatId') })
  }

  const authoredMessages = messages.map(m => ({...m, user: { _id: m.authorId }}))
  if(isFetching) {
    return <Spinner />
  } else {
    return (
      <>
        <GiftedChat
          messages={authoredMessages}
          onSend={messages => OnSend(messages)}
          inverted={false}
          user={{
            _id: props.user.id,
            name: props.user.email,
            avatar: props.user.avatarUrl
          }}
        />
        { Platform.OS === 'android' ? <KeyboardSpacer /> : null }
      </>
    );
  }
}
