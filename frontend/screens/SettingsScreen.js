import React from 'react';
import { View, StyleSheet } from 'react-native';
import FormButton from '../components/FormButton';
import { HeaderOptions } from '../components/Header';
import Colors from '../constants/Colors';

export default function SettingsScreen(props) {
  return(
    <View style={styles.container}>
      <View style={styles.form}>
        <FormButton label={'Sign out'} onPress={() => props.signOut()} />
      </View>
    </View>
  )
}

SettingsScreen.navigationOptions = {
  ...HeaderOptions({ title: 'Settings' })
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.headerTextColor,
    alignItems: "center",
    justifyContent: "space-between"
  },
  form: {
    flex: 1,
    justifyContent: "center",
    width: "80%"
  },
})
