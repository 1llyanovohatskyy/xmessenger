import React from 'react';
import {
  StyleSheet,
  View
} from 'react-native';
import { HeaderOptions } from '../components/Header';
import ChatListContainer from '../containers/ChatListContainer';

export default function HomeScreen(props) {
  return (
    <View style={styles.container}>
      <ChatListContainer navigation={props.navigation} />
    </View>
  );
}

HomeScreen.navigationOptions = {
  ...HeaderOptions({ title: 'XMessanger' })
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});
