import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native';
import FormTextInput from '../components/FormTextInput';
import FormButton from '../components/FormButton'
import Colors from '../constants/Colors';
import { MonoText } from '../components/StyledText';

export default function SignUpScreen(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return(
    <View style={styles.container}>
      {/* <Image source={imageLogo} style={styles.logo} /> */}
      <View style={styles.form}>
        <FormTextInput
          value={email}
          onChangeText={value => setEmail(value)}
          placeholder={'Email'}
        />
        <FormTextInput
          secureTextEntry={true}
          value={password}
          onChangeText={value => setPassword(value)}
          placeholder={'Password'}
        />
        <FormButton
          label={'Sign up'}
          onPress={() => props.register(email, password, props.navigation)}
        />
        <View style={{ alignItems: "center", justifyContent: "center" }}>
          <MonoText style={{fontSize: 18}}>or</MonoText>
          <TouchableOpacity onPress={() => props.navigation.navigate('SignIn')}>
            <Text style={styles.alternativeText}>
              Sign in
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.headerTextColor,
    alignItems: "center",
    justifyContent: "space-between"
  },
  logo: {
    flex: 1,
    width: "100%",
    resizeMode: "contain",
    alignSelf: "center"
  },
  form: {
    flex: 1,
    justifyContent: "center",
    width: "80%"
  },
  alternativeText: {
    color: Colors.tintColor,
    fontSize: 18,
    borderBottomWidth: 1,
    borderBottomColor: Colors.tintColor
  }
});

