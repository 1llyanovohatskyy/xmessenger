import React from 'react';
import { HeaderOptions } from '../components/Header';
import CreateChatContainer from '../containers/CreateChatContainer';

export default function CreateScreen() {
  return <CreateChatContainer />
}

CreateScreen.navigationOptions = {
  ...HeaderOptions({ title: 'Create chat' })
};

