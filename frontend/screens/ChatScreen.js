import React from 'react';
import {
  StyleSheet,
  View
} from 'react-native';
import { HeaderOptions } from '../components/Header';
import ChatContainer from '../containers/ChatContainer';

export default function ChatScreen(props) {
  return (
    <View style={styles.container}>
      <ChatContainer navigation={props.navigation} />
    </View>
  );
}

ChatScreen.navigationOptions = {
  ...HeaderOptions({ title: 'XMessanger' })
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});
