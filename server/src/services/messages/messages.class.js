const { Service } = require('feathers-mongoose');

exports.Messages = class Messages extends Service {
    create(data, params) {
        const authUser = params.user;
        data.authorId = authUser._id;
        return super.create(data, params);
    }
};
