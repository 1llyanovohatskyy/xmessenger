const users = require('./users/users.service.js');
const messages = require('./messages/messages.service.js');

const chats = require('./chats/chats.service.js');

const userChats = require('./user-chats/user-chats.service.js');

const chatVisits = require('./chat-visits/chat-visits.service.js');

const pushTokens = require('./push-tokens/push-tokens.service.js');

// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(messages);
  app.configure(chats);
  app.configure(userChats);
  app.configure(chatVisits);
  app.configure(pushTokens);
};
