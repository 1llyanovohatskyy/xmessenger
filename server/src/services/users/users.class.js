const { Service } = require('feathers-mongoose');

const crypto = require('crypto');

const avatarServiceUrl = 'https://s.gravatar.com/avatar';
const query = 's=60';

exports.Users = class Users extends Service {
  create(data, params) {
    const { email, password } = data;
    const hash = crypto.createHash('md5').update(email.toLowerCase()).digest('hex');
    const avatarUrl = `${avatarServiceUrl}/${hash}?${query}`;

    const userData = {
      email,
      password,
      avatarUrl: avatarUrl,
    };

    return super.create(userData, params);
  }
};
