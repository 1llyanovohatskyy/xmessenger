// Initializes the `chat-visits` service on path `/chat-visits`
const { ChatVisits } = require('./chat-visits.class');
const createModel = require('../../models/chat-visits.model');
const hooks = require('./chat-visits.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/chat-visits', new ChatVisits(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('chat-visits');

  service.hooks(hooks);
};
