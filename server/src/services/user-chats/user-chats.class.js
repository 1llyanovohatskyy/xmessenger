const { Service } = require('feathers-mongoose');

exports.UserChats = class UserChats extends Service {
  constructor(options, app) {
    super(options, app);
    this.app = app;
  }

  create(data, params) {
    const authUser = params.user;

    const userChatData = {
      name: data.name,
      members: [authUser._id, ...data.members],
    };

    return super.create(userChatData);
  }

  async find(params) {
    const authUser = params.user;
    const app = this.app;

    const chats = await app.service('chats').find({
      query: {
        members: authUser._id,
      }
    });

    const messageFutures = chats.data.map(async function (chat) {
      const message = await app.service('messages').find({
        query: {
          chatId: chat._id,
          $sort: {
            createdAt: -1,
          },
          $limit: 1,
        }
      });

      chat.lastMessage = message.data.length === 0 ? null : message.data[0];
    });

    await Promise.all(messageFutures);

    const unreadMessagesCountFutures = chats.data.map(async function (chat) {
      const visit = await app.service('chat-visits').find({
        query: {
          chatId: chat._id,
          userId: authUser._id,
          $sort: {
            createdAt: -1,
          },
          $limit: 1,
        }
      });

      const lastVisit = visit.data.length === 0 ? null : visit.data[0].createdAt;
      chat.lastVisit = lastVisit;
      if (lastVisit !== null) {
        const messages = await app.service('messages').find({
          query: {
            chatId: chat._id,
            createdAt: {
              $gt: Date.parse(lastVisit),
            },
          },
        });
        chat.unreadMessageCount = messages.data.length;
      } else {
        chat.unreadMessageCount = 0;
      }
    });

    await Promise.all(unreadMessagesCountFutures);

    return chats;
  }

  async get(id, params) {
    await this.app.service('chat-visits').create({
      chatId: id,
      userId: params.user._id,
    });

    return this.app.service('messages').find({
      query: {
          chatId: id,
          $sort: {
            createdAt: 1,
          }
      }
    })
  }
};
