// Initializes the `userChats` service on path `/userChats`
const { UserChats } = require('./user-chats.class');
const createModel = require('../../models/user-chats.model');
const hooks = require('./user-chats.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/userChats', new UserChats(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('userChats');

  service.hooks(hooks);
};
