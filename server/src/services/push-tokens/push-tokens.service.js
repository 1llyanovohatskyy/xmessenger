// Initializes the `push-tokens` service on path `/push-tokens`
const { PushTokens } = require('./push-tokens.class');
const createModel = require('../../models/push-tokens.model');
const hooks = require('./push-tokens.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/push-tokens', new PushTokens(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('push-tokens');

  service.hooks(hooks);
};
